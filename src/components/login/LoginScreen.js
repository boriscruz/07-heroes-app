import React, { useContext } from 'react'
import { types } from '../../types/types';
import { AuthContext } from '../../auth/AuthContext';

const LoginScreen = ({ history }) => {

    const { dispatch } = useContext(AuthContext);

    const handleClick = () => {

        const lastPath = localStorage.getItem('lastPath') || '/';

        dispatch({
            type: types.login,
            payload: {
                name: 'Roo'
            }
        });
        history.replace(lastPath);
    }

    return (
        <div className="container">
            <h1>Login</h1>
            <hr />
            <button
                className="btn btn-primary"
                onClick={handleClick}
            >
                Login
            </button>
        </div>
    )
}

export default LoginScreen
